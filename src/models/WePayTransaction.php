<?php

namespace AppealingStudio\WepayManagement;

use Illuminate\Database\Eloquent\Model;

class WePayTransaction extends Model
{
	/**
	 * We should define table name in this case
	 * 
	 * @var string
	 */
	protected $table = 'wepay_transactions';

	/**
	 * All attributes guarded by default.
	 *
	 * @var array
	 */
	protected $guarded = array('*');

	////////////////////////////////////////////////////////////////////
	/////////////////////////// RELATIONSHIPS //////////////////////////
	////////////////////////////////////////////////////////////////////

	/**
	 * Get the Transaction related to the Wepay account activity
	 *
	 * @return Tasting
	 */
	public function transaction()
	{
		return $this->belongsTo(Config::get('wepay-management::dbrelations.users_model'));
	}

}
