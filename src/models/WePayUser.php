<?php

namespace AppealingStudio\WepayManagement;

use Illuminate\Database\Eloquent\Model;

class WePayUser extends Model
{
	/**
	 * We should define table name in this case
	 * 
	 * @var string
	 */

	protected $table = 'wepay_users';

	/**
	 * All attributes guarded by default.
	 *
	 * @var array
	 */
	protected $guarded = array('*');

	////////////////////////////////////////////////////////////////////
	/////////////////////////// RELATIONSHIPS //////////////////////////
	////////////////////////////////////////////////////////////////////

	/**
	 * Get the app User related to the WePay User
	 *
	 * @return Tasting
	 */
	public function user()
	{
		return $this->belongsTo(Config::get('wepay-management::dbrelations.users_model'));
	}

	////////////////////////////////////////////////////////////////////
	//////////////////////////// AUTHENTICATION ////////////////////////
	////////////////////////////////////////////////////////////////////

	/**
	 * Get wepay user details
	 * Access token must be from the user who owns the account
	 *  
	 * @param  string $access_token
	 * @return object
	 */
	public function getUserDetails($access_token)
	{
		$wepay = new Wepay($access_token);

		return $wepay->request('user');
	}

	/**
	 * Get wepay account details
	 * Access token must be from the user who owns the account
	 * 
	 * @param  string $access_token
	 * @return object
	 */
	public function getAccountDetails($access_token)
	{
		$wepay = new Wepay($access_token);

		return $wepay->request('account');
	}

}
