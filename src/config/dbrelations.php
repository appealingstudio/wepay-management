<?php

/**
 * WePay database relationships
 */

return array(

	// Related users table parameters
	'users_model' 		 => 'User',
	'users_table_name'	 => 'users',
	'user_id_field_name' => 'user_id',

	// Related transactions/payment table parameters
	'transactions_model' 		=> 'Transaction'
	'transactions_table_name'	=> 'transactions',
	'transaction_id_field_name' => 'transaction_id',

);
