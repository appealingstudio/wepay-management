<?php

/**
 * WePay error management
 */

return array(

	// Message triggered on exception error
	'exception_message' => 'Something was wrong, please contact us if the problem persist',

	// Where should be redirected the app in case of exception ?
	'exception_redirect' => array(
		'request_exception' => 'exception',
		'permission_exception' => 'exception',
		'server_exception' => 'exception'
	)
);
