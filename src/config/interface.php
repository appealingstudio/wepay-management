<?php

/**
 * Payment Interface class name implemented by WePayManagement
 */

return array(
	'fullNamespace' => 'CorkSharing\Interfaces\PaymentInterface',
	'name' => 'PaymentInterface'
);