<?php

/**
 * WePay Management payment options
 */

return array(

	// Account description
	'account_description' => 'CorkSharing payment account',

	// Payment fees
	'fee_percentage' 	=> 15,
	'fee_payer'		 	=> 'payee',
);
