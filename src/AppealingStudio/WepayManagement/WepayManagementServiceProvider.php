<?php

namespace AppealingStudio\WepayManagement;

use Illuminate\Support\ServiceProvider;

class WepayManagementServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('appealing-studio/wepay-management');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// App container
		$this->app->bind('Illuminate\Container\Container', function ($app)
		{
			return $app;
		});

		// Register main class
		$this->app['WepayManagement'] = $this->app->share(function($app)
		{
			return new WepayManagement($this->app);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('WepayManagement');
	}

}