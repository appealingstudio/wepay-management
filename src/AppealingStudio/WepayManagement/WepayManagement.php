<?php

namespace AppealingStudio\WepayManagement;

use Illuminate\Container\Container;

// Package models
use AppealingStudio\WepayManagement\WePayUser;
use AppealingStudio\WepayManagement\WePayTransaction;

// Wepay SDK resources
use WePay;
use WePayServerException;
use WePayRequestException;
use WePayPermissionException;

class WepayManagement
{
	/**
	 * Class variables
	 */
	protected $app;
	protected $wepay;
	protected $credentials;
	protected $exceptionUrl;

	// --------------------------------------------------------------------

	/**
	 * Build a new WePay instace.
	 */
	public function __construct(Container $app)
	{
		$this->app = $app;
		$this->credentials = $this->app['config']->get('wepay-management::credentials');

		// By default, go to previous page on exception error
		$this->exceptionUrl = $this->app['url']->previous();
		
		// Try to set the environment
		try
		{
			Wepay::useStaging($this->credentials['client_id'], $this->credentials['client_secret']);
		}
		catch (RuntimeException $e)
		{
			// Catch environment set up exception
		}

		$this->_exceptionErrors();
		$this->wepay = new Wepay($this->credentials['access_token']);
	}

	// --------------------------------------------------------------------

	/**
	 * Create WePay user account.
	 *
	 * This is performed in three steps:
	 * 
	 * 1. Redirect user to WePay sign up page and get returned code after submission.
	 * 2. Get OAuth2 access token.
	 * 3. Create WePay account.
	 *
	 * All steps are processed from within the same URL.
	 * 
	 * @param  Model $user
	 * @param  array $input
	 * @param  string $redirectUrl
	 * @param  string $userFullName
	 * @param  string $referenceID
	 * @param  Redirect $successUrl
	 * @return Redirect
	 */
	public function createPayoutUserAccount($user, $input, $redirectUrl, $userFullName, $referenceID, $successUrl)
	{
		if (empty($input))
		{
			// Step 1.
			return $this->payoutUserAccountSignUp($redirectUrl);
		}
		else
		{
			if (isset($input['code']))
			{
				// Step 2.
				$accessToken = $this->getAccessToken($redirectUrl, $input['code']);

				// Step 3.
				return $this->createAccount($user, $accessToken, $userFullName, $referenceID, $successUrl);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Show WePay sign up page..
	 * 
	 * @param  Request $redirectUrl
	 * @return Redirect
	 */
	public function payoutUserAccountSignUp($redirectUrl)
	{
		return $this->app['redirect']->to('https://stage.wepay.com/v2/oauth2/authorize?' .
			'client_id=' . $this->credentials['client_id'] .
			'&redirect_uri=' . $redirectUrl .
			'&scope=manage_accounts,view_balance,collect_payments,' .
			'view_user,send_money,preapprove_payments,manage_subscriptions');
	}

	// --------------------------------------------------------------------

	/**
	 * Get access token from OAuth2.
	 * 
	 * @param  string $redirectUrl
	 * @param  string $code
	 * @return object
	 */
	public function getAccessToken($redirectUrl, $code)
	{
		$wepay = new WePay($this->credentials['access_token']);

		return $wepay->request(
			'oauth2/token',
			array(
				'client_id'        => $this->credentials['client_id'],
				'client_secret'    => $this->credentials['client_secret'],
				'redirect_uri'     => $redirectUrl,
				'code' 		       => $code
			)
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Create WePay Account.
	 * Access token must be from the user who will own the account.
	 * 
	 * @param  object $user
	 * @param  string $accessToken
	 * @param  string $userFullName
	 * @param  string $referenceID
	 * @param  Redirect $successUrl
	 * @return Redirect
	 */
	public function createAccount($user, $accessToken, $userFullName, $referenceID, $successUrl)
	{
		$wepay = new Wepay($accessToken->access_token);

		$wepayAccount = $wepay->request(
			'account/create',
			array(
				'name'     		=> $userFullName,
				'reference_id'	=> $referenceID,
				'description' 	=> $this->app['config']->get('wepay-management::payment.account_description')
			)
		);

		if (isset($user->wepay->id))	// Update account
		{
			$wepayUser = $user->wepay;
		}
		else 	// Create account
		{
			$wepayUser = new WePayUser;
		}

		$wepayUser->wepay_user_id = $accessToken->user_id;
		$wepayUser->wepay_account_id = $wepayAccount->account_id;
		$wepayUser->wepay_account_uri = $wepayAccount->account_uri;
		$wepayUser->oauth2_access_token = $accessToken->access_token;

		$user->wepay()->save($wepayUser);

		return $successUrl;
	}

	// --------------------------------------------------------------------

	/**
	 * Get WePay account details from database.
	 * 
	 * @param  Model $user
	 * @return Model
	 */
	public function getDBPayoutAccount($user)
	{
		return $user->wepay;
	}

	// --------------------------------------------------------------------

	/**
	 * Get WePay account details from payment service.
	 * 
	 * @param  Model $dbAccountDetails
	 * @return object
	 */
	public function getUserAccountDetails($dbAccountDetails)
	{
		$wepay = new Wepay($dbAccountDetails->oauth2_access_token);

		return $wepay->request(
			'account',
			array(
				'account_id' => $dbAccountDetails->wepay_account_id
			)
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Get WePay user profile details from payment service.
	 * 
	 * @param  Model $dbAccountDetails
	 * @return object
	 */
	public function getUserProfileDetails($dbAccountDetails)
	{
		$wepay = new Wepay($dbAccountDetails->oauth2_access_token);

		return $wepay->request('user');
	}

	// --------------------------------------------------------------------

	/**
	 * Authorize credit card for charging later
	 * 
	 * @param  array $transactionData
	 * @return WePayTransaction
	 */
	public function authorizeCreditCard($transactionData, $redirectUrl)
	{
		$this->exceptionUrl = $redirectUrl;

		// Create credit card
		$creditCard = $this->createCreditCard($transactionData);

		$this->app['log']->info('WePay: Authorizing credit card', (array) $creditCard);

		$authorization = $this->wepay->request('credit_card/authorize', array(
				'client_id'        => $this->credentials['client_id'],
				'client_secret'    => $this->credentials['client_secret'],
				'credit_card_id'   => $creditCard->credit_card_id,
			)
		);

		$this->app['log']->info('WePay: Credit card authorized', (array) $authorization);

		$wepayTransaction = new WePayTransaction;

		$wepayTransaction->wepay_email = $authorization->email;
		$wepayTransaction->wepay_credit_card_id = $authorization->credit_card_id;
		$wepayTransaction->wepay_credit_card_user_name = $authorization->user_name;
		$wepayTransaction->wepay_credit_card_name = $authorization->credit_card_name;

		return $wepayTransaction;
	}

	// --------------------------------------------------------------------

	/**
	 * Get credit card info
	 * 
	 * @param  Transaction $transaction
	 * @return object
	 */
	public function getCreditCardInfo($wepayTransaction)
	{
		$this->app['log']->info('WePay: Getting credit card info', $wepayTransaction->toArray());

		return $this->wepay->request(
			'credit_card',
			array(
				'client_id'        => $this->credentials['client_id'],
				'client_secret'    => $this->credentials['client_secret'],
				'credit_card_id'   => $wepayTransaction->wepay_credit_card_id,
			)
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Delete authorized credit card
	 * 
	 * @param  string $creditCardId
	 * @return object
	 */
	public function deleteCreditCard($wepayTransaction)
	{
		$this->app['log']->info('WePay: Delete credit card', $wepayTransaction->toArray());

		return $this->wepay->request(
			'credit_card/delete',
			array(
				'client_id'        => $this->credentials['client_id'],
				'client_secret'    => $this->credentials['client_secret'],
				'credit_card_id'   => $wepayTransaction->wepay_credit_card_id,
			)
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Pay a host after accepting a transaction
	 *
	 * @param  Transaction $transaction
	 *
	 * @return Redirect
	 */
	public function chargeCreditCard($transactionData)
	{
		$wepay = new Wepay($transactionData['access_token']);
		$fee_percentage = $this->app['config']->get('wepay-management::payment.fee_percentage');
		$fee_amount = ($transactionData['amount'] * $fee_percentage) / 100;

		return $wepay->request(
			'checkout/create',
			array(
				'account_id'          => $transactionData['account_id'],
				'amount'              => $transactionData['amount'],
				'short_description'   => $transactionData['short_description'],
				'type'                => 'EVENT',
				'payment_method_id'   => $transactionData['payment_method_id'],
				'payment_method_type' => 'credit_card',
				'app_fee'			  => $fee_amount,
				'fee_payer'			  => $this->app['config']->get('wepay-management::payment.fee_payer')
			)
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Creates a credit card from a Transaction
	 *
	 * @param  array $transaction
	 * @return object
	 */
	protected function createCreditCard($transaction_input)
	{
		$this->app['log']->info('WePay: Creating credic card...');

		return $this->wepay->request(
			'credit_card/create',
			array(
				'client_id'        => $this->credentials['client_id'],
				'cc_number'        => $transaction_input['card_number'],
				'cvv'              => $transaction_input['security_code'],
				'expiration_month' => $transaction_input['expiration_month'],
				'expiration_year'  => $transaction_input['expiration_year'],
				'user_name'        => $transaction_input['card_name'],
				'email'            => $transaction_input['email'],
				'address'          => array(
					'address1' => $transaction_input['address'],
					'address2' => '',
					'city'     => $transaction_input['city'],
					'state'    => $transaction_input['state'],
					'zip'      => $transaction_input['zip'],
					'country'  => $transaction_input['country'],
				),
			)
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Manage WePay service exception errors
	 *
	 * @return void
	 */
	private function _exceptionErrors()
	{
		$this->app->error(function (WePayRequestException $exception, $code)
		{
			$this->_log('WePay Request Exception', $exception);

			return $this->_redirect($exception, 'request_exception');
		});

		$this->app->error(function (WePayPermissionException $exception, $code)
		{
			$this->_log('WePay Permission Exception', $exception);
			return $this->_redirect($exception, 'permission_exception');
		});

		$this->app->error(function (WePayServerException $exception, $code)
		{
			$this->_log('WePay Server Exception', $exception);
			return $this->_redirect($exception, 'server_exception');
		});
	}

	// --------------------------------------------------------------------

	/**
	 * On exception error, app is redirected to the url provided at package 'errors' config file.
	 * 
	 * The redirection includes:
	 *  - Input
	 *  - Last Url
	 *  - Exception code & message
	 * 
	 * @return Redirect
	 */
	private function _redirect($exception, $exceptionType)
	{
		$redirectTo = $this->app['config']->get('wepay-management::errors.exception_redirect.' . $exceptionType);

		$this->app['log']->info($redirectTo);

		return $this->app['redirect']->to($redirectTo)
			->withInput()
			->with('previous_url', $this->app['url']->previous())
			->with('exception', ['message' => $exception->getMessage(), 'code' => $exception->getCode()]);
	}

	// --------------------------------------------------------------------

	/**
	 * Log exception errors
	 * 
	 * @param  string $exceptionType
	 * @param  object $exception
	 * @return void
	 */
	private function _log($exceptionType, $exception)
	{
		$this->app['log']->error(
			$exceptionType,
			array(
				'code' => $exception->getCode(),
				'message' => $exception->getMessage(),
			)
		);		
	}

}
