<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWepayTransactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wepay_transactions', function (Blueprint $table)
		{
			$table->increments('id');

				// App transaction_id field with FK to app transaction id field on main transactions table
				//$table->integer(Config::get('wepay-management::dbrelations.transaction_id_field_name'));
				$table->integer('transaction_id');

				// Authorized credit card id
				$table->integer('wepay_credit_card_id')->nullable();
				$table->string('wepay_credit_card_name')->nullable();
				$table->string('wepay_credit_card_user_name')->nullable();
				$table->string('wepay_email')->nullable();

				// Checkout
			    $table->integer('wepay_checkout_id')->nullable();
			    $table->string('wepay_checkout_uri')->nullable();

			    // Preapproval
			    $table->integer('wepay_preapproval_id')->nullable();
			    $table->string('wepay_preapproval_uri')->nullable();

			    // Withdrawal
			    $table->integer('wepay_withdrawal_id')->nullable();
			    $table->string('wepay_withdrawal_uri')->nullable();

		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wepay_transactions');
	}

}