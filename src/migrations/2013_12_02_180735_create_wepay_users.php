<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWepayUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wepay_users', function (Blueprint $table)
		{
			$table->increments('id');

				// App user_id field with FK to app user id field on main users table
				//$table->integer(Config::get('wepay-management::dbrelations.user_id_field_name'));
				$table->integer('user_id');

				// WePay user id
				$table->integer('wepay_user_id')->nullable();

				// OAuth2 access_token
				$table->string('oauth2_access_token')->nullable();

				// Wepay account
				$table->string('wepay_account_id')->nullable();
				$table->string('wepay_account_uri')->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wepay_users');
	}

}